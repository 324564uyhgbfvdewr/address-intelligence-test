import Vue from "vue";
import VueRouter from "vue-router";

import HomeView from '@/views/HomeView'
import PhotoView from '@/views/PhotoView'
import UserView from '@/views/UserView'
import AuthView from '@/views/AuthView'
import FavouritesView from '@/views/FavouritesView'
import SearchView from '@/views/SearchView'

Vue.use(VueRouter);

export const router = new VueRouter({
    routes: [
        { path: '', name: 'Home', component: HomeView },
        { path: '/faves', name: 'Favourites', component: FavouritesView },
        { path: '/photo/:id', name: 'Photo', component: PhotoView },
        { path: '/user/:username', name: 'User', component: UserView },
        { path: '/auth', name: 'Auth', component: AuthView },
        { path: '/search', name: 'Search', component: SearchView },
    ]
});

export default router
